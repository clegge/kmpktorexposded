package sample.service

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.selectAll
import sample.db.DatabaseFactory.dbQuery
import sample.dto.BookDTO
import sample.model.Book
import sample.model.Books

class BookService {
    suspend fun getAll(): List<BookDTO> = dbQuery {
        Book.all().map { it.toDto() }
    }

    suspend fun getById(id:Int): BookDTO = dbQuery {
        Book[id].toDto()
    }
}