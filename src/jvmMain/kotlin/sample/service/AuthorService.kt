package sample.service

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.selectAll
import sample.db.DatabaseFactory.dbQuery
import sample.dto.AuthorDTO
import sample.dto.BookDTO
import sample.model.Author
import sample.model.Book
import sample.model.Books

class AuthorService {
    suspend fun getAll(): List<AuthorDTO> = dbQuery {
        Author.all().map { it.toDto() }
    }

    suspend fun getById(id:Int): AuthorDTO = dbQuery {
        Author[id].toDto()
    }
}