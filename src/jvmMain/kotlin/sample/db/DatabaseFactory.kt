package sample.db

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.config.HoconApplicationConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import sample.model.Author
import sample.model.Authors
import sample.model.Book
import sample.model.Books
import java.util.*

//
object DatabaseFactory {
    private val appConfig = HoconApplicationConfig(ConfigFactory.load())
    private val db_port = appConfig.property("db.port").getString()
    private val db_name = appConfig.property("db.name").getString()
    private val db_user = appConfig.property("db.name").getString()
    private val db_password = appConfig.property("db.dbPassword").getString()
    private val dbUrl = appConfig.property("db.jdbcUrl").getString().replace("PORT", db_port).replace("NAME", db_name)

    fun init(){
        Database.connect(hikari())
        transaction {
            SchemaUtils.drop (Books, Authors)
            create(Books)
            create(Authors)
        }
        loadAuthors()
        loadBooks()
    }

    private fun loadAuthors(){
        transaction {
            val sKing = Author.new {
                nameFirst =  "Steven"
                nameLast = "King"
                dateCreated = Date().time
                dateUpdated = Date().time
            }
        }
    }

    private fun loadBooks(){
        transaction {
            val sKing = Author.all().first()
            Book.new {
                title = "The Shining"
                pageCount = 345
                dateCreated = Date().time
                dateUpdated = Date().time
                author = sKing
            }
            Book.new {
                title = "Salems Lot"
                pageCount = 234
                dateCreated = Date().time
                dateUpdated = Date().time
                author = sKing
            }
            Book.new {
                title = "Kujo"
                pageCount = 420
                dateCreated = Date().time
                dateUpdated = Date().time
                author = sKing
            }

            Book.new {
                title = "The Shining"
                pageCount = 564
                dateCreated = Date().time
                dateUpdated = Date().time
                author = sKing
            }
        }
    }

    private fun hikari(): HikariDataSource {
        val config = HikariConfig()
        config.driverClassName = "org.postgresql.Driver"
        config.jdbcUrl = dbUrl
        config.username = db_user
        config.password = db_password
        config.maximumPoolSize = 3
        config.isAutoCommit = false
        config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        config.validate()
        return HikariDataSource(config)
    }

    suspend fun <T> dbQuery(block: () -> T): T =
        withContext(Dispatchers.IO) {
            transaction { block() }
        }
}