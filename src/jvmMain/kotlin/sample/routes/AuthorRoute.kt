package sample.routes


import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.*
import sample.service.AuthorService


fun Route.author(service: AuthorService) {
    route("/author"){
        get("/") {
            call.respond(service.getAll())
        }
        get("/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalStateException("Must provide id")
            val dto = service.getById(id)
            if (dto == null) call.respond(HttpStatusCode.NotFound)
            else call.respond(dto)
        }
    }
}