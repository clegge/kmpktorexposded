package sample

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType

import io.ktor.routing.Routing
import io.ktor.serialization.DefaultJsonConfiguration
import io.ktor.serialization.serialization
import io.ktor.server.engine.commandLineEnvironment


import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.serialization.json.Json
import sample.db.DatabaseFactory
import sample.routes.author
import sample.routes.book
import sample.service.AuthorService
import sample.service.BookService

fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        serialization(
            contentType = ContentType.Application.Json,
            json = Json(
                DefaultJsonConfiguration.copy(
                    prettyPrint = true
                )
            )
        )
    }
    DatabaseFactory.init()
    install(Routing) {
        book(BookService())
        author(AuthorService())
    }
}


fun main(args: Array<String>) {
    embeddedServer(Netty, commandLineEnvironment(args)).start()
}
