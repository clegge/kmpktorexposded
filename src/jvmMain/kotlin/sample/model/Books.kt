package sample.model



import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import sample.dto.AuthorDTO
import sample.dto.BookDTO
import sample.dto.MinDTO

object Books : IntIdTable() {
    val title = varchar("title", 255)
    val pageCount = integer("pageCount")
    val dateCreated = long("dateCreated")
    val dateUpdated = long("dateUpdated")
    val author = reference("author", Authors.id)
}

class Book(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Book>(Books)
    var title by Books.title
    var pageCount by Books.pageCount
    var dateCreated by Books.dateCreated
    var dateUpdated by Books.dateUpdated
    var author by Author referencedOn Books.author

    fun toDto() = BookDTO(
        id = id.value,
        title = title,
        author = author.toMinDTO()
    )

    fun toMinDTO() = MinDTO(
        id = id.value,
        description = title
    )
}



