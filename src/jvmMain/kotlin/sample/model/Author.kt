package sample.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Table
import sample.dto.AuthorDTO
import sample.dto.BookDTO
import sample.dto.MinDTO
import sample.model.Books.autoIncrement

object Authors : IntIdTable() {
    val nameFirst = varchar("nameFirst", 255)
    val nameLast = varchar("nameLast", 255)
    val dateCreated = long("dateCreated")
    val dateUpdated = long("dateUpdated")
}

class Author(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Author>(Authors)
    var nameFirst by Authors.nameFirst
    var nameLast by Authors.nameLast
    var dateCreated by Authors.dateCreated
    var dateUpdated by Authors.dateUpdated
    val books  by Book referrersOn Books.author

    fun toMinDTO() = MinDTO(
        id = id.value,
        description = "$nameFirst $nameLast"
    )

    fun toDto() =AuthorDTO(
        id = id.value,
        nameFirst = nameFirst,
        nameLast = nameLast,
        books = books.map { it.toMinDTO() }
    )
}

