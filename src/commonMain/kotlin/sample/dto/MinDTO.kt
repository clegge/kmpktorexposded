package sample.dto

import kotlinx.serialization.Serializable

@Serializable
data class MinDTO (
    val id:Int,
    val description:String
)

