package sample.dto

import kotlinx.serialization.Serializable

@Serializable
data class BookDTO(
    val id:Int,
    val title:String,
    val author:MinDTO
)