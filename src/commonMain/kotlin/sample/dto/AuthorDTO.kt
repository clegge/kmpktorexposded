package sample.dto

import kotlinx.serialization.Serializable

@Serializable
data class AuthorDTO(
    val id:Int,
    val nameFirst:String,
    val nameLast:String,
    val books:List<MinDTO>
)